DataConverter
=========

DataConverter is a toolkit for converting data between any formats.

Install
-------

First make sure you have installed the latest version of  [node.js](http://nodejs.org)
(You may need to restart your computer after this step).

From NPM:

    mpm install @prof-itgroup/data-converter
    
Model
-----

The model configuration allows to configure the data scheme that can be handled by converter and the data format that will be received on output.

The model is a array of fields which are named `Node`. A `Node` contains next fields:

```json
{
    "to": "position.coordinates.2",
    "from": "height",
    "defaultValue": 0,
    "type": "number",
  }
```

>key - String, required
>
>type: 'string', 'number' or 'boolean', required
>
>from: String, optional
>
>defaultValue: String, Number, Boolean or null, optional

Example
-------

```javascript
const { converter } = require('@prof-itgroup/data-converter');

const model = [
  {
    to: 'position.coordinates.0',
    from: 'lat',
    type: 'number',
  },
  {
    to: 'position.coordinates.1',
    from: 'lon',
    type: 'number',
  },
  {
    to: 'position.coordinates.2',
    from: 'height',
    defaultValue: 0,
    type: 'number',
  },
  {
    to: 'position.type',
    defaultValue: 'Point',
    type: 'string',
  },
  {
    to: 'speed',
    from: 'speed',
    type: 'number',
  },
];

console.log(converter(model, { lat: 52.123234, lon: 32.434345, speed: 100 })); // { position: { type: 'Point', coordinates: [52.123234, 32.434345, 0], }, speed: 100 };
```

You can get prebuilt function using `factory` function:

```javascript
const { converter, factory } = require('@prof-itgroup/data-converter');

const model = [
  {
    to: 'position.coordinates.0',
    from: 'lat',
    type: 'number',
  },
  {
    to: 'position.coordinates.1',
    from: 'lon',
    type: 'number',
  },
  {
    to: 'position.coordinates.2',
    from: 'height',
    defaultValue: 0,
    type: 'number',
  },
  {
    to: 'position.type',
    defaultValue: 'Point',
    type: 'string',
  },
  {
    to: 'speed',
    from: 'speed',
    type: 'number',
  },
];

const handler = factory(converter, model); // prebuilt converter

console.log(handler({ lat: 52.123234, lon: 32.434345, speed: 100 })); // { position: { type: 'Point', coordinates: [52.123234, 32.434345, 0], }, speed: 100 };
```