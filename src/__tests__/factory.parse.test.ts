import { converter, factory } from '..';

import { IDataConvertModel, INode } from '../lib/types';

const model: IDataConvertModel = [
  {
    from: 'lat',
    to: 'position.coordinates.0',
    type: 'number',
  } as INode<number>,
  {
    from: 'lon',
    to: 'position.coordinates.1',
    type: 'number',
  } as INode<number>,
  {
    defaultValue: 0,
    from: 'height',
    to: 'position.coordinates.2',
    type: 'number',
  } as INode<number>,
  {
    defaultValue: 'Point',
    to: 'position.type',
    type: 'string',
  } as INode<string>,
  {
    from: 'speed',
    to: 'speed',
    type: 'number',
  } as INode<number>,
];

test('FACTORY CONVERTER', () => {
  expect(factory(converter, model)({ lat: 52.123234, lon: 32.434345, speed: 100 })).toEqual({
    position: {
      coordinates: [52.123234, 32.434345, 0],
      type: 'Point',
    },
    speed: 100,
  });
});
