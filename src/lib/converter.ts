import { get, set } from 'lodash';

import { IDataConvertModel } from './types';

import { toType } from './toType';

export const converter = <R, T>(model: IDataConvertModel, data: R): T => {
  return model.reduce((res, { type, from, to, defaultValue }): Partial<T> => {
    const value = from ? get(data, from, defaultValue) : defaultValue;
    set(res, to, type && value !== undefined ? toType(type, value) : value);
    return res;
  }, {}) as T;
};
