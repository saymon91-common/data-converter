import { BuiltConverter, Converter, IDataConvertModel } from './types';

export const factory = <R, T>(converter: Converter<R, T>, model: IDataConvertModel): BuiltConverter<R, T> => {
  return converter.bind(null, model);
};
