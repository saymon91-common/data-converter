import { DataTypes } from './types';

export const toType = <T extends number | string | boolean>(type: DataTypes, value: any): T => {
  switch (type) {
    case 'boolean':
      return !!value as T;
    case 'number':
      const res = Number(value);

      if (isNaN(res)) {
        throw new Error(`Error data converting: ${type}:${value}`);
      }

      return res as T;
    case 'string':
      return value.toString();
    default:
      throw new Error(`Unknown target data type`);
  }
};
