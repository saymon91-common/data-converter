export type Converter<R, T> = (model: IDataConvertModel, data: R) => T;

export type BuiltConverter<R, T> = (data: R) => T;

export type DataTypes = 'string' | 'number' | 'boolean';

export interface INode<T extends number | string | boolean = any> {
  to: string;
  defaultValue?: T | null;
  type?: DataTypes;
  from?: string;
}

export type IDataConvertModel = INode[];
